setwd("~/Documents/RASR_base")

source("MainCode/Evaluator_function.R")
library('plot.matrix')

domains = c("riverswim","inventory","population")#,"population_small"

for (domain in domains){
  # Evaluate domains (In the train and test setting)
  stats = EvalDomain(folder_name = "Tabular Experiment/Domains/" ,stats_output_folder = wdir("Tabular Experiment/TrainingEvaluation/"),domain = domain,alpha_begin = exp(4) ,alpha_end = exp(-15))
  Pi_Exact = apply(stats[,which(startsWith( colnames(stats), 'A_Pi'))], 2, as.character)
  Naive_Pi = apply(stats[,which(startsWith( colnames(stats), 'C_Pi'))], 2, as.character)
  Diff_Pi = apply(stats[,which(startsWith( colnames(stats), 'A_Pi'))] - stats[,which(startsWith( colnames(stats), 'C_Pi'))], 2, as.character)
  dimnames(Pi_Exact) = list(rownames = round(as.numeric(rownames(stats)),4) ,colnames = 1:length(which(startsWith( colnames(stats), 'A_Pi'))))
  dimnames(Naive_Pi) = dimnames(Pi_Exact)
  dimnames(Diff_Pi) = dimnames(Pi_Exact)
  PolicyPlotDir = "Tabular Experiment/TrainingEvaluation/ERMPolicyPlots/"
  # bmp(paste0(PolicyPlotDir,domain,"_naive_ERM.bmp"), width=16, height=10,units="in",res = 100)
  pdf(file = paste0(PolicyPlotDir,domain,"_naive_ERM.pdf"))
  plot(Naive_Pi,border=NA, col = colorRamps::matlab.like(length(unique(c(Naive_Pi)))), ylab="Risk level (alpha) [log scale]", xlab="State",main=paste0(domain," naive_ERM policy"))
  dev.off()

  # bmp(paste0(PolicyPlotDir,domain,"_exact_ERM.bmp"), width=16, height=10,units="in",res = 100)
  pdf(file = paste0(PolicyPlotDir,domain,"_exact_ERM.pdf"))
  plot(Pi_Exact,border=NA, col = colorRamps::matlab.like(length(unique(c(Pi_Exact)))), ylab="Risk level (alpha) [log scale]", xlab="State",main=paste0(domain," exact_ERM policy"))
  dev.off()

  # bmp(paste0(PolicyPlotDir,domain,"_diff_ERM.bmp"), width=16, height=10,units="in",res = 100)
  pdf(file = paste0(PolicyPlotDir,domain,"_diff_ERM.pdf"))
  plot(Diff_Pi,border=NA, col = colorRamps::matlab.like(length(unique(c(Diff_Pi)))), ylab="Risk level (alpha) [log scale]", xlab="State",main=paste0(domain," diff_ERM (Exact_ERM - Naive_ERM) policy"))
  dev.off()
}

# for (domain  in domains){
#   statistics_summary(domain = domain)
# }
# stats_df = read.csv(paste0("Tabular Experiment/TestingEvaluation/",domain,"_stats.csv"), row.names = 1)



