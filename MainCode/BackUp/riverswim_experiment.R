source("/home/jh1111/Documents/RASR/dermfunction.R")
library(gsubfn) 

# Preprocess Data
mdp.df = read.csv("/home/jh1111/Documents/RASR/domains/riverswim/training.csv",header = TRUE)
outcomes = sort(unique(mdp.df$idoutcome)) # Outcome space
mdp = lapply(outcomes, function(o) prep_MDP(mdp.df[mdp.df$idoutcome==o,]))
# Change option set mdp organization
list[S,A,lSl,lS2l,lAl,P,R] = mdp[[1]]
P = lapply(outcomes, function(o) mdp[[o+1]]$P)
Rew = lapply(outcomes, function(o) mdp[[o+1]]$R)

discount = read.csv("/home/jh1111/Documents/RASR/domains/riverswim/parameters.csv",header = TRUE)$value
S_0 = read.csv("/home/jh1111/Documents/RASR/domains/riverswim/initial.csv",header = TRUE)

# Initialize algorithm variable
epsilon = exp(-20)
alpha_0 = exp(10)
Time = ceiling(log(alpha_0/epsilon)/(1-discount))
V = matrix( 0 , nrow = Time+1 , ncol = nrow(S_0) )
Pbar = Reduce("+",P)/length(outcomes)

# Calculate ERM(reward) and also ERM(Value function)
R = list()
for (t in 0:Time){
  R[[t+1]] = Rew[[1]]*0
  for (s in  1:lSl){for (a in  1:lAl){for (s2 in  1:lS2l){
    R[[t+1]][s,a,s2] = ERM(sapply(outcomes, function(o) Rew[[o+1]][s,a,s2]),alpha = alpha_0*discount^t)
  }}}
}

Pi = V*0
for (i in 1:1000){
  V[Time+1,] = sapply(1:lSl, function(s)  max(sapply(1:lAl, function(a)  Pbar[s,a,] %*% (R[[Time+1]][s,a,] + discount * V[Time+1,]) )  ))
}
Pi[Time+1,] = sapply(1:lSl, function(s)  which.max(sapply(1:lAl, function(a)  Pbar[s,a,] %*% (R[[Time+1]][s,a,] + discount * V[Time+1,])  ) ))

for (t in Time:1){
  V[t, ] = sapply(1:lSl, function(s)  max(sapply(1:lAl, function(a)  ERM( c(R[[t]][s,a,] + discount * V[t+1,]),alpha = alpha_0*discount^(t-1),prob=Pbar[s,a,])   )))
  Pi[t, ] = sapply(1:lSl, function(s)  which.max(sapply(1:lAl, function(a)  ERM( c(R[[t]][s,a,] + discount * V[t+1,]),alpha = alpha_0*discount^(t-1),prob=Pbar[s,a,])   )))
}

Scores = sapply(1:(Time+1),function (t) S_0$probability %*% V[t,])

# Read and preprocess test set
mdp.test = read.csv("/home/jh1111/Documents/RASR/domains/riverswim/test.csv",header = TRUE)
outcomes.test = sort(unique(mdp.test$idoutcome)) # Outcome space
mdpT = lapply(outcomes.test, function(o) prep_MDP(mdp.test[mdp.test$idoutcome==o,]))
P.test = lapply(outcomes.test+1, function(o) mdpT[[o]]$P)
Rew.test = lapply(outcomes.test+1, function(o) mdpT[[o]]$R)

# Evaluate on test case with RASR manner
# Calculate RASR-ERM(reward) and also RASR-ERM(Value function)
V.RASR.test = matrix( 0 , nrow = Time+1 , ncol = nrow(S_0) )
Pbar.RASR.test = Reduce("+",P.test)/length(outcomes.test)
R.RASR.test = list()
for (t in 0:Time){
  R.RASR.test[[t+1]] = Rew.test[[1]]*0
  for (s in  1:lSl){for (a in  1:lAl){for (s2 in  1:lS2l){
    R.RASR.test[[t+1]][s,a,s2] = ERM(sapply(outcomes.test+1, function(o) Rew.test[[o]][s,a,s2]),alpha = alpha_0*discount^t)
  }}}
}

for (i in 1:1000){
  V.RASR.test[Time+1,] = sapply(1:lSl, function(s)  Pbar.RASR.test[s,Pi[Time+1,s],] %*% (R.RASR.test[[Time+1]][s,Pi[Time+1,s],] + discount * V.RASR.test[Time+1,]) )
}

for (t in Time:1){
  V.RASR.test[t, ] = sapply(1:lSl, function(s) ERM( c(R.RASR.test[[t]][s,Pi[t,s],] + discount * V.RASR.test[t+1,]),alpha = alpha_0*discount^(t-1),prob=Pbar.RASR.test[s,Pi[t,s],])   )
}

Scores.RASR.test = sapply(1:(Time+1),function (t) S_0$probability %*% V.RASR.test[t,])


# Evaluate Static Epistemic risk, Dynamic Aleatory risk

V.test = list()

for (o in outcomes.test+1){
  P.cur = P.test[[o]]
  R.cur = Rew.test[[o]]
  V.test[[o]] = V*0
  # for time 0 we calculate P.cur.Pi and R.cur.Pi cause it is used to iterate for many times at expectation.
  P.cur.Pi = t(sapply(1:lSl,function(s) P.cur[s,Pi[Time+1,s],]))
  R.cur.Pi = t(sapply(1:lSl,function(s) R.cur[s,Pi[Time+1,s],]))
  for (i in 1:1000){
    V.test[[o]][Time+1,] = sapply(1:lSl, function(s)  P.cur.Pi[s,] %*% (R.cur.Pi[s,] + discount * V.test[[o]][Time+1,]) )  
  }
  
  for (t in Time:1){
    V.test[[o]][t, ] = sapply(1:lSl, function(s)  ERM( c(R.cur[s,Pi[t,s],] + discount * V.test[[o]][t+1,]),alpha = alpha_0*discount^(t-1),prob=P.cur[s,Pi[t,s],])   )
  }
}
test.Scores = lapply(outcomes.test+1, function(o) sapply(1:(Time+1),function (t) S_0$probability %*% V.test[[o]][t,]) )

ERM.test.Scores = sapply(1:(Time+1), function(t)  ERM(sapply(outcomes.test+1, function(o) test.Scores[[o]][t] ),alpha = alpha_0*discount^(t-1)) ) 
E.test.Scores =  sapply(1:(Time+1), function(t)  E(sapply(outcomes.test+1, function(o) test.Scores[[o]][t] )) ) 

stats = cbind(Scores,Scores.RASR.test,E.test.Scores,ERM.test.Scores)

rownames(stats) = c(sapply(1:(Time), function(t) alpha_0*discount^(t-1) ),0)
colnames(stats) = c("Train RASR Scores","Test RASR Scores","E_Static_Epis(ERM(Dyn_Alea))","ERM_Static_Epis(ERM(Dyn_Alea))")

write.csv(stats, "/home/jh1111/Documents/RASR/domains/riverswim_stats.csv")


