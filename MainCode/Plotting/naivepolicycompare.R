setwd("~/Documents/RASR_base")
source("MainCode/Evaluator_function.R")
library('plot.matrix')

folder_name = "Tabular Experiment/Domains/" 
stats_output_folder = wdir("Tabular Experiment/TrainingEvaluation/")

alpha_begin = exp(4) 
alpha_end = exp(-15)

domains = c("population_small","population") # ,"riverswim","population_small","population"
for (domain in domains){
  list[S,A,Rew,R,P,Pbar,lSl,lS2l,lAl,Time,discount,levels,alpha_0,Pi_Exact,V_train,S_0,Scores.train] = ExactRASR(paste0(folder_name,domain),NULL,"/training.csv",alpha_begin,alpha_end)
  
  # Value_Naive_RASR(levels,rep(0,lSl),R[[t]],Pbar,Pi[t,],lSl,lAl,discount,S_0)
  True_score_Naive_RASR = function(levels,V_cur,R,Pbar,Pi_cur,lSl,lAl,discount,S_0){
    n = length(levels)
    Scores_cur = rep(0,n)
    # names(Scores_cur) = levels
    for (i in 1:500){
      V_cur = sapply(1:lSl, function(s) Pbar[s,Pi_cur[s],]%*%(R[[n]][s,Pi_cur[s],] + discount * V_cur))
    }
    for (i in n:1){
      V_cur = sapply(1:lSl, function(s) ERM( c(R[[i]][s,Pi_cur[s],] + discount * V_cur),alpha = levels[i],prob=Pbar[s,Pi_cur[s],]) )
      Scores_cur[i] = ERM(V_cur,alpha = levels[i],prob=S_0$probability) 
    }
    
    return(Scores_cur)
  }
  
  
  stats = EvalDomain(folder_name = "Tabular Experiment/Domains/" ,
                     stats_output_folder = wdir("Tabular Experiment/TrainingEvaluation/"),
                     domain = domain,alpha_begin = exp(4) ,alpha_end = exp(-15))
  
  Pi_Exact = data.matrix(stats[,which(startsWith( colnames(stats), 'A_Pi'))])
  Naive_Pi = data.matrix(stats[,which(startsWith( colnames(stats), 'C_Pi'))])
  # levels = as.numeric(rownames(stats))
  Exact_Obj = stats[,which(startsWith( colnames(stats), 'True_train'))]
  Naive_Obj = stats[,which(startsWith( colnames(stats), 'Naive_train'))]
  nom_index= length(levels)
  
  TrueNaive_Obj = rep(0,nom_index)
  All_Naive_Obj = list()
  names(TrueNaive_Obj) = levels
  SeenNaive_Pi = c()
  for (i in 1:nom_index){
    if (i > 1){
      if (!sum(sapply(1:nrow(SeenNaive_Pi),function(j) sum(SeenNaive_Pi[j,] == Naive_Pi[i,])==lSl ))){
        All_Naive_Obj[[as.character(nrow(SeenNaive_Pi))]] =  True_score_Naive_RASR(levels,rep(0,lSl),R,Pbar,Naive_Pi[i,],lSl,lAl,discount,S_0)
        SeenNaive_Pi = rbind(SeenNaive_Pi,Naive_Pi[i,])
      }
    } else {
      All_Naive_Obj[[as.character(0)]] =  True_score_Naive_RASR(levels,rep(0,lSl),R,Pbar,Naive_Pi[i,],lSl,lAl,discount,S_0)
      SeenNaive_Pi = rbind(SeenNaive_Pi,Naive_Pi[i,])
    }
    
    TrueNaive_Obj[i]=All_Naive_Obj[[which(sapply(1:nrow(SeenNaive_Pi),function(j) sum(SeenNaive_Pi[j,] == Naive_Pi[i,])==lSl ))]][i]
  }
  elim_index = which.min(Naive_Obj[-nom_index] < max(Naive_Obj[-nom_index])*0.999)
  # ERM Suboptimality
  df_sub = rbind(data.frame(alpha = levels[-nom_index],Objective_val = Exact_Obj[-nom_index],algorithm = "Exact_ERM"),
                 data.frame(alpha = levels[-nom_index],Objective_val = TrueNaive_Obj[-nom_index],algorithm = "TrueNaive_ERM"),
                 data.frame(alpha = levels[-nom_index],Objective_val = Naive_Obj[-nom_index],algorithm = "Naive_ERM"))
  
  pdf(file = paste0("Tabular Experiment/TestingEvaluation/NaiveSuboptimalityPlot/",domain,"_SubObjNaive.pdf"), width = 16)
  print(ggplot(data = df_sub, aes(x=alpha, y=Objective_val,group=algorithm,color=algorithm)) + geom_line() +
          scale_x_continuous(trans='log2',breaks = signif(levels[(1:floor(length(levels)/30))*30],2)) + 
          theme(text = element_text(size = 35),axis.text = element_text(size = 30),panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
                panel.background = element_blank(), axis.line = element_line(colour = "black"))  + ggtitle("") +
          ylab("Objective value") + xlab("Alpha (log scale)"))
  dev.off()
  
  # EVAR Suboptimality
  dfbeta = c()
  dfalgo = c()
  dfalpha = c()
  for (risk in c(0.99,(19:1)*0.05 )){
    Exact_EVAR = Exact_Obj + log(1-risk)/levels
    Naive_EVAR = Naive_Obj + log(1-risk)/levels
    TrueNaive_EVAR = TrueNaive_Obj + log(1-risk)/levels
    dfbeta = c(dfbeta,rep(risk,3))
    dfalgo = c(dfalgo,"Naive_EVAR","Exact_EVAR","TrueNaive_EVAR")
    dfalpha = c(dfalpha,levels[which.max(Naive_EVAR)],levels[which.max(Exact_EVAR)],levels[which.max(TrueNaive_EVAR)])
  }
  df = data.frame(beta=dfbeta,algorithm=dfalgo,alpha=dfalpha)
  
  pdf(file = paste0("Tabular Experiment/TestingEvaluation/NaiveSuboptimalityPlot/",domain,"_SubAlphaBeta.pdf"), width = 12)
  print(ggplot(data = df, aes(x=beta, y=alpha,group=algorithm,color=algorithm)) + geom_line() + scale_y_continuous(trans='log2',breaks = signif(levels[(1:floor(length(levels)/10))*10],2)) +
          theme(text = element_text(size = 35),axis.text = element_text(size = 30),panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
                panel.background = element_blank(), axis.line = element_line(colour = "black"))  + ggtitle("") +
          ylab("Alpha (log scale)") + xlab("Beta")
          ) 
  dev.off()
  
  # Respective Policy matrix
  
}



