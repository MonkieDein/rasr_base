---
title: "convexity"
author: "Monkie"
date: "3/28/2022"
output: pdf_document
---

```{r, include=FALSE}
setwd("~/Desktop/GITHUB/rasr_base")
# import library
source("MainCode/RASR_code.R")
library(ggplot2)
library(dplyr)
set.seed(6)
```



In this document we wanted to investigate the convexity of ERM with respect to risk level $\alpha$. 
1. We randomly generate (n) different distribution of $X^{\pi_i} = R_i^Tw_i ~,~ \forall i \in 0..n$. 
```{r}
# random distribution generator
gen_w = function(lSl){
  m = sample(1:lSl ,1)
  out = rep(0,lSl)
  index = sample(1:lSl, m)
  # generate distribution and normalize to sum of 1
  prob = runif(m,min=1e-10)
  prob = prob/sum(prob)
  out[index] = prob
  return(out)
}
```


```{r}
n = 10
lSl = 3
lrl = 5
r_min = 0
r_max = 100
R = lapply(1:n, function(i) matrix(runif(lSl*lrl,r_min,r_max),nrow = lSl,ncol = lrl) )
w_s = lapply(1:n, function(i) gen_w(lSl))
X = lapply(1:n,function(i) t(R[[i]])%*%w_s[[i]])
w_r = lapply(1:n, function(i) gen_w(lrl))
Alpha = (1:100000)*0.00001
Values = lapply(1:n,function(i) cbind(Alpha, sapply(Alpha,function(a) ERM(X[[i]],a,w_r[[i]]) ),i ) )
val = as.data.frame(Reduce('rbind',Values))
```

plot
```{r}
val %>% ggplot(aes(x=Alpha,y=V2,group = i,color=i))+ geom_line()
```

We observe the possibility of alternating selection 
```{r}
inspect <- val %>% filter(i %in% c(7,9))
inspect %>% ggplot(aes(x=Alpha,y=V2,group = i,color=i))+ geom_line()
```
Note for really low risk $\alpha<0.0246$ i7 is better, $0.6695>\alpha>0.0246$ i9 is better, for $\alpha > 0.6695$ the i7 overflip i9 again.


```{r}
cat(X[[7]],"---",w_r[[7]])
cat("\n",X[[9]],"---",w_r[[9]])
```
7n9max
```{r}
V7M9 = as.data.frame(Values[[7]])
V7M9[,2] = pmax(Values[[7]][,2],Values[[9]][,2])
V7M9 %>% ggplot(aes(x=Alpha,y=V2))+ geom_line()

```



