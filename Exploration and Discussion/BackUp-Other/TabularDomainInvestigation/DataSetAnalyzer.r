
# Import function
setwd("~/Documents/RASR_base")
source("MainCode/Evaluator_function.R")

domain = "inventory"
file = "/test.csv"
get_mdp_param = function(folder_name,file){
  mdp.df = read.csv( paste0(folder_name,file)  ,header = TRUE)
  outcomes = sort(unique(mdp.df$idoutcome)) # Outcome space
  mdp = lapply(outcomes, function(o) prep_MDP(mdp.df[mdp.df$idoutcome==o,]))
  # Change option set mdp organization
  list[S,A,lSl,lS2l,lAl,P,R] = mdp[[1]]
  P = lapply(outcomes+1, function(o) mdp[[o]]$P)
  Rew = lapply(outcomes+1, function(o) mdp[[o]]$R)
  discount = read.csv(paste0(folder_name,"/parameters.csv"),header = TRUE)$value
  S_0 = read.csv(paste0(folder_name,"/initial.csv"),header = TRUE)
  Pbar = Reduce("+",P)/length(outcomes)
  return(list(S = S,A = A,Rew = Rew,R = R,P = P, Pbar = Pbar,lSl = lSl,lS2l = lS2l,lAl = lAl,outcomes=outcomes))
}


# Explore inconsistency within training data sets and testing data sets.

stats = NULL
domains = c("riverswim","inventory","population","population_small")
for (domain in domains){
  folder_name = paste0("Tabular Experiment/Domains/",domain,"/")
  
  list[S,A,Rew,R,P,Pbar,lSl,lS2l,lAl,outcomes] = get_mdp_param(folder_name,"training.csv")
  list[S_test,A_test,Rew_test,R_test,P_test,Pbar_test,lSl_test,lS2l_test,lAl_test,outcomes_test] =  get_mdp_param(folder_name,"test.csv")
  
  Rbar = Reduce("+",Rew)/length(outcomes)
  Rbar_test = Reduce("+",Rew_test)/length(outcomes_test)
  
  Rsd = lapply(Rew, function(x) (x-Rbar)^2)
  Rsd = sqrt(Reduce("+",Rsd)/length(outcomes))
  
  Rsd_test = lapply(Rew_test, function(x) (x-Rbar_test)^2)
  Rsd_test = sqrt(Reduce("+",Rsd_test)/length(outcomes_test))
  cur_stat = data.frame(max_RowSumPbar_diff = max(sapply(1:lAl, function(a) rowSums(abs(Pbar[,a,] - Pbar_test[,a,])))),
                        max_RowSumRbar_diff = max(sapply(1:lAl, function(a) rowSums(abs(Rbar[,a,] - Rbar_test[,a,]) ))),
                        max_RowSumRtrain_val = max(sapply(1:lAl, function(a) rowSums( abs(Rbar[,a,]) ))) ,
                        max_RowSumR_normalize_diff = max(sapply(1:lAl, function(a) rowSums(abs(Rbar[,a,] - Rbar_test[,a,]))/(rowSums(abs(Rbar[,a,]))+1e-10) )))
  stats = rbind(stats,cur_stat)
}


rownames(stats) = domains
stats

